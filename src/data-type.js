
export class DataType {
  constructor(name) {
    this.name = name;
  }

  get getName() {
    return this.name;
  }
}

export class CharType extends DataType{
  constructor(n = null){
    super('CHAR');
    this.n = n;
  }

  get getN() {
    return this.n;
  }
}

export class TextType extends DataType{
  constructor(n = null){
    super('TEXT');
    this.n = n;
  }

  get getN() {
    return this.n;
  }
}


export class IntegerType extends DataType{
  constructor( ){
    super('INTEGER');    
  }
}

export class DoubleType extends DataType{
  constructor( ){
    super('DOUBLE');    
  }
}

export class DateType extends DataType{
  constructor( ){
    super('DATE');    
  }
}

export class DatetimeType extends DataType{
  constructor( ){
    super('DATETIME');    
  }
}

export class BooleanType extends DataType{
  constructor( ){
    super('BOOLEAN');    
  }
}
