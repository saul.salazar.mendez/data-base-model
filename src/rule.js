export class RuleBase {
  constructor(type) {
    this.type = type;
  }

  get getType() {
    return type;
  }
}

export class UniqueRule extends RuleBase {
  constructor() {
    super('UNIQUE');
  }
}

export class NotNullRule extends RuleBase {
  constructor() {
    super('NOT NULL');
  }
}

export class PrimaryKeyRule extends RuleBase {
  constructor() {
    super('PRIMARY KEY');
  }
}

export class DefaultRule extends RuleBase {
  constructor(defaultValue) {
    super('UNIQUE');
    this.defaultValue = defaultValue;
  }

  get getDefaultValue() {
    return this.defaultValue;
  }
}

export class AutoIncrementRule extends RuleBase {
  constructor() {
    super('AUTO INCREMENT');
  }
}

export class ForeingKeyRule extends RuleBase {
  /**
   * 
   * @param {*} key Nombre del campo
   * @param {*} tableName Tabla target
   * @param {*} foreingKey campo de la tabla target
   */
  constructor(key, tableName, foreingKey) {
    super('FOREIGN KEY');
    this.key = key;
    this.tableName = tableName;
    this.foreingKey = foreingKey;
  }

  get getTableName() {
    return this.getTableName;
  }

  get getForeingKey() {
    return this.foreingKey;
  }

  get getKey() {
    return this.key;
  }
}