export class Atribute {
  constructor(name, type, rules) {
    this.name = name;
    this.type = type;
    this.rules = rules;
  }

  get getName() {
    return this.name;
  }

  get getRules() {
    return this.rules;
  }

  get getType() {
    return this.type;
  }
}
