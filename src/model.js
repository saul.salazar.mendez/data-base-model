import { DatetimeType } from "./data-type.js";
import { DefaultRule } from "./rule.js";

export class Model {
  constructor(name) {
    this.name = name;
    this.created = new Atribute('created', new DatetimeType(), [ new DefaultRule(Date.now())] );
    this.updated = new Atribute('created', new DatetimeType(), [new DefaultRule(Date.now())] );
  }
}