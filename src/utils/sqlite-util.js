import { Model } from "./../model.js";
import { RuleBase } from "../rule.js";

export function getSQLITEschema(listModels) {
    let schema = '';
    for (const model of listModels) {
        if (model instanceof Model) {
            schema += createTable(model);
        }
    }
}

function createTable(model){
    if (model instanceof Model) {
        let table = 'CREATE TABLE IF NOT EXISTS suppliers (';    
        let keys = Object.keys(model);
        for (const key of keys) {
            if (model[key] instanceof Atribute) {
                table += getTextAtribute(model[key]);
            }
        }
        table += ');'
        return table;
    }
    return '';
    
    
}

function getTextAtribute(atribute) {
    if ( atribute instanceof Atribute){
        return `    ${atribute.getName} ${atribute.getType.getName} ${getRules(atribute.getRules)}`;
    }
}

function getRules(rules) {
    out = '';
    for (const rule of rules) {
        if ( rule instanceof RuleBase) {
            out += getRuleText(rule) + ' ';
        }
    }
    out += ',\n';
    return out;
}

function getRuleText(rule) {
    switch (rule.constructor.name) {
        case 'DefaultRule':            
            return `${rule.getType} ${rule.getDefaultValue}`;    
        default:
            return rule.getType;
    }
}

function getForeingkeys(atribute, rules) {
    out = '';
    for (const rule of rules) {
        if ( rule instanceof ForeingKeyRule) {
            out += `     FOREIGN KEY (${atribute.getName}) REFERENCES ${rule.getTableName}(${rule.getForeingKey}),\n`;
        }
    }
    out += ';\n';
    return out;
}